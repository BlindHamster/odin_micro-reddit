class User < ApplicationRecord
  has_many :posts
  has_many :comments
  before_save { self.email = email.downcase}

  validates :name, presence: true, uniqueness: { case_sensitive: false }, length: 3...20
  validates :email, presence: true, uniqueness: { case_sensitive: false }, length: 6...100
end
