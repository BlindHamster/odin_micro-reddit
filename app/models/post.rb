class Post < ApplicationRecord
  belongs_to :user
  has_many :comments

  validates :body, presence: true, length: 5...5000
  validates :user_id, presence: true
end
