require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: 'Anonimus', email: 'anonimus@mail.com')
  end

  test 'should be valid' do
    assert @user.valid?
  end

  test 'name too short' do
    @user.name = "Ge"
    assert_not @user.valid?
  end

  test 'name too long' do
    @user.name = "G" * 21
    assert_not @user.valid?
  end

  test 'email too long' do
    @user.email = "G" * 101
    assert_not @user.valid?
  end

  test 'email too short' do
    @user.email = "email"
    assert_not @user.valid?
  end

  test 'name empty' do
    @user.name = " "
    assert_not @user.valid?
  end

  test 'email empty' do
    @user.email = " "
    assert_not @user.valid?
  end

  test 'name duplicated' do
    duplicate_user = @user.dup
    duplicate_user.email = "Different@email.com"
    @user.save
    assert_not duplicate_user.valid?
  end

  test 'email duplicated' do
    duplicate_user = @user.dup
    duplicate_user.name = "Different name"
    @user.save
    assert_not duplicate_user.valid?
  end
end
