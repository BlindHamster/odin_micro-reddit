require 'test_helper'

class PostTest < ActiveSupport::TestCase

  def setup
    @user = User.create(name: 'Anonimus', email: 'anonimus@mail.com')
    @post = @user.posts.new(body: "Some awesome text!")
  end

  test 'should be valid' do
    assert @post.valid?
  end

  test 'body too short' do
    @post.body = "aa"
    assert_not @post.valid?
  end

  test 'body too long' do
    @post.body = "a" * 5001
    assert_not @post.valid?
  end

  test 'body empty' do
    @post.body = ""
    assert_not @post.valid?
  end

  test 'no user id' do
    @post.user_id = nil
    assert_not @post.valid?
  end
end
