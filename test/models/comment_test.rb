require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  def setup
    @user = User.create(name: 'Anonimus', email: 'anonimus@mail.com')
    @post = @user.posts.create(body: "Some awesome text!")
    @comment = @user.comments.new(post_id: @post.id, body: 'Comment body text!')
  end

  test 'should be valid' do
    assert @comment.valid?
  end

  test 'body too short' do
    @comment.body = "aa"
    assert_not @comment.valid?
  end

  test 'body too long' do
    @comment.body = "a" * 501
    assert_not @comment.valid?
  end

  test 'body empty' do
    @comment.body = ""
    assert_not @comment.valid?
  end

  test 'no user id' do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test 'no post id' do
    @comment.post_id = nil
    assert_not @comment.valid?
  end
end
